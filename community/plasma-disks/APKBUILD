# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-disks
pkgver=6.1.2
pkgrel=0
pkgdesc="Monitors S.M.A.R.T. capable devices for imminent failure"
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="
	kirigami
	smartmontools
	"
makedepends="
	extra-cmake-modules
	kauth-dev
	kcmutils-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	kservice-dev
	qt6-qtbase-dev
	samurai
	solid-dev
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/plasma/plasma-disks.git"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-disks-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run -a ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c56eb494fd079bc5139f97374d0aeff8b05c229e08ecbe4fb51aa846ab7e8b3235cd089969fc031ba0fd0783d0fb0d571f6f970704d69585e453467533cc3916  plasma-disks-6.1.2.tar.xz
"
