# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=bluedevil
pkgver=6.1.2
pkgrel=0
pkgdesc="Integrate the Bluetooth technology within KDE workspace and applications"
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND ( LGPL-2.1-only OR LGPL-3.0-only )"
depends="
	bluez
	kded
	obexd
	"
makedepends="
	bluez-qt-dev
	extra-cmake-modules
	kcmutils-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kded-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libplasma-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	samurai
	shared-mime-info
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/plasma/bluedevil.git"
source="https://download.kde.org/stable/plasma/$pkgver/bluedevil-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a866b565d61df899f1c148847e531d533638f24716f6b515c44ba7aedd39b3f774fab06b6d464a52b24109f06a9ffae5a6edab4f3279118170a3535de6746420  bluedevil-6.1.2.tar.xz
"
